from collections import OrderedDict

import pytest

import renew


class NoCtor(renew.Mold):
    pass


class NoArguments(renew.Mold):
    def __init__(self):
        pass


class BadArguments(renew.Mold):
    def __init__(self, argument_name):
        self.different_name = argument_name


def test_creppy():
    assert repr(NoCtor()) == "NoCtor()"
    assert repr(NoArguments()) == "NoArguments()"


class Reproducible(renew.Mold):

    def __init__(self, item_name, *other_arguments):
        self.item_name = item_name
        self.other_arguments = other_arguments


@pytest.mark.parametrize("statement", [
    "Reproducible(None, None)",
    "Reproducible('things')",
    "Reproducible('things', 1)",
    "Reproducible('things', 1, 2, 'nothing', 'much', 'more', None)",
])
def test_it_is_reproducible(statement):
    a = eval(statement)
    assert repr(a) == statement


def test_reproducible():
    outer = Reproducible(
        NoArguments(),
        Reproducible("this one should", 'fit inline'),
        Reproducible(
            "some string",
            Reproducible(1, 2, None),
            Reproducible((1, 2, 3), 'b6589fc6ab0dc82cf12099d1c2d40ab994e8410c',
                         'ddfe163345d338193ac2bdc183f8e9dcff904b43'),
            Reproducible([11, 12, 13, 14, 15], {}, ()),
        ),
        None,
        Reproducible("here one with dict", {
            "aa": "a_value",
            "bb": 23,
            34: None,
        }),
        Reproducible("this one will break", [
            "87acec17cd9dcd20a716cc2cf67417b71c8a7016",
            "36a27136f3015f5ed0e1fe268ad7a93a985196cf",
            "a8624bd0a5caa95e0be9cd95d4ed86a558a33553",
            "73986020e8eb281b34b88661bea3fbcd14318e3e",
        ]),
        {
            "dict_check": Reproducible(
                "Pythons dicts are not supported and this test case is",
                "just to prove how bad it is. It's syntactically valid, but",
                "you would never say that's nice (indentation fail).",
            ),
            "another key": "To ensure at least keys are sorted",
            23: "Simple items have a little chance to be ok.",
        },
    )

    expected_reproduction = """\
Reproducible(
    NoArguments(),
    Reproducible('this one should', 'fit inline'),
    Reproducible(
        'some string',
        Reproducible(1, 2, None),
        Reproducible(
            (1, 2, 3),
            'b6589fc6ab0dc82cf12099d1c2d40ab994e8410c',
            'ddfe163345d338193ac2bdc183f8e9dcff904b43',
        ),
        Reproducible([11, 12, 13, 14, 15], {}, ()),
    ),
    None,
    Reproducible('here one with dict', {34: None, 'aa': 'a_value', 'bb': 23}),
    Reproducible(
        'this one will break',
        [
            '87acec17cd9dcd20a716cc2cf67417b71c8a7016',
            '36a27136f3015f5ed0e1fe268ad7a93a985196cf',
            'a8624bd0a5caa95e0be9cd95d4ed86a558a33553',
            '73986020e8eb281b34b88661bea3fbcd14318e3e',
        ],
    ),
    {
        23: 'Simple items have a little chance to be ok.',
        'another key': 'To ensure at least keys are sorted',
        'dict_check': Reproducible(
            'Pythons dicts are not supported and this test case is',
            "just to prove how bad it is. It's syntactically valid, but",
            "you would never say that's nice (indentation fail).",
        ),
    },
)"""
    assert repr(outer) == expected_reproduction
    assert repr(eval(expected_reproduction)) == expected_reproduction


def test_bad_namespace_definition():
    msg = "_._cls_namespace has to be a nonempty string or None, got 'tuple'."
    with pytest.raises(ValueError, match=msg):
        class _(renew.Mold):
            _cls_namespace = ("insane",)

            def __init__(self):
                """ coverage does not collect dosctrings """

    with pytest.raises(ValueError, match=msg):
        class __(renew.Mold):
            _cls_namespace = ("insane",)


def test_bad_definition():
    class BadDefinition(renew.Mold):
        def __init__(self, valid_name):
            self.different_name = valid_name

    msg = "'BadDefinition' object has no attribute 'different_name'"
    with pytest.raises(AttributeError, match=msg):
        repr(BadDefinition("ok"))


def test_renew_set():
    r = Reproducible(
        {
            '87acec17cd9dcd20a716cc2cf67417b71c8a7016',
            '36a27136f3015f5ed0e1fe268ad7a93a985196cf',
            'a8624bd0a5caa95e0be9cd95d4ed86a558a33553',
            '73986020e8eb281b34b88661bea3fbcd14318e3e',
        },
        [{1, 2, 3}, {4, 5, 6, 7, 8}],
    )
    assert repr(r) == """\
Reproducible(
    {
        '36a27136f3015f5ed0e1fe268ad7a93a985196cf',
        '73986020e8eb281b34b88661bea3fbcd14318e3e',
        '87acec17cd9dcd20a716cc2cf67417b71c8a7016',
        'a8624bd0a5caa95e0be9cd95d4ed86a558a33553',
    },
    [{1, 2, 3}, {4, 5, 6, 7, 8}],
)"""


def test_repr_inheritance():
    class Derived(Reproducible):
        pass

    a = Derived(
        "89777c66e3f39bfe15392bb3d1c6ec51d98f8071",
        "4f1030137e25e64fd6798cbe18bd99ff64e8c94f",
        "fca200d95659e7375c5418e7bbd887dce1b11d9f",
        "2b45ed32d40683c1673bf4aaade17a55a51fbba1",
    )
    assert repr(a) == """\
Derived(
    '89777c66e3f39bfe15392bb3d1c6ec51d98f8071',
    '4f1030137e25e64fd6798cbe18bd99ff64e8c94f',
    'fca200d95659e7375c5418e7bbd887dce1b11d9f',
    '2b45ed32d40683c1673bf4aaade17a55a51fbba1',
)"""


@pytest.mark.parametrize("expression", [
    "[]", "{}", "()", "[1, 2]", "{1, 2}", "(1, 2)", "'just a string'",
    "[{1, 2, 3}, [(4, [(5, 6), 7], 8), 9.01], {10: 11, 12: 13}]", """\
{
    '327603e5417941eefa1b3a25f831a50b73375c59': 'fe11383d9e79d97573f33fb682de070ce1c706d4',
    'a96da3b51126afaba9acf291d8bb934d0cbe5905': '44a4978104be4f6a824acbf9784bb364a575fb1f',
}""", """\
{
    '327603e5417941eefa1b3a25f831a50b73375c59': {
        'fe11383d9e79d97573f33fb682de070ce1c706d4': [{1, 2, 3}, [(4, [(5, 6), 7], 8), 9.01], {10: 11, 12: 13}],
    },
}"""])
def test_repr_plain_types(expression):
    assert renew.reproduction(eval(expression)) == expression


class Defaults(renew.Mold):
    def __init__(self, first, second=22, third=33, fourth=4.4):
        self.first, self.second, self.third, self.fourth = first, second, third, fourth


@pytest.mark.parametrize('instance, expected_repr', [
    (Defaults(1), "Defaults(1)"),
    (Defaults(1, 22), "Defaults(1)"),
    (Defaults(1, 22, 33), "Defaults(1)"),
    (Defaults(1, 22, 33, None), "Defaults(1, fourth=None)"),
    (Defaults(1, 10, 20, 30), "Defaults(1, 10, 20, 30)"),
    (Defaults(None, None, None, None), "Defaults(None, None, None, None)"),
    (Defaults(1, fourth='d', second='b', third=None), "Defaults(1, 'b', None, 'd')"),
    (Defaults(1, fourth='droppin'), "Defaults(1, fourth='droppin')"),
])
def test_default_args(instance, expected_repr):
    assert repr(instance) == expected_repr
    assert repr(eval(expected_repr)) == expected_repr


class Keywords(renew.Mold):
    def __init__(self, x=1, **kw_args):
        self.x = x
        self.kw_args = kw_args


def test_reprs_with_kw_args():
    assert repr(Keywords(e=5, d=4, c=3, b=2)) == "Keywords(b=2, c=3, d=4, e=5)"
    assert repr(Keywords()) == "Keywords()"
    assert repr(Keywords(2, null=None)) == "Keywords(2, null=None)"


def test_raises_bad_kw_definition():
    class Kws(renew.Mold):
        def __init__(self, **kw):
            self.kw = list(kw.items())

    msg = "Expecting Kws.kw attribute to be a dict or OrderedDict, got list."
    with pytest.raises(AttributeError, match=msg):
        renew.reproduction(Kws(kak=1))


def test_inheritance(tmpdir):
    class Car(renew.Mold):
        _cls_namespace = "cars"
        _cls_dependency = "that.things"

        def __init__(self, body_type, engine_power, fuel, seats, color=None):
            self.body_type = body_type
            self.engine_power = engine_power
            self.fuel = fuel
            self.seats = seats
            self.color = color

    class Driver(renew.Mold):
        _cls_namespace = "persons"
        _cls_dependency = "living.things"

        def __init__(self, first_name, last_name, *cars):
            self.first_name = first_name
            self.last_name = last_name
            self.cars = cars

    car_1 = Car("Truck", 120.0, "diesel", 2)
    car_2 = Car("Van", 145.0, "diesel", seats=7, color="silver")
    car_3 = Car("Roadster", 210.0, "gasoline", seats=2)

    driver_1 = Driver("Blenda", "Klapa", car_1)
    driver_2 = Driver("Trytka", "Blotnick", car_2, car_3)

    assert repr(driver_1) == "persons.Driver('Blenda', 'Klapa', cars.Car('Truck', 120.0, 'diesel', 2))"
    assert repr(driver_2) == """\
persons.Driver(
    'Trytka',
    'Blotnick',
    cars.Car('Van', 145.0, 'diesel', 7, 'silver'),
    cars.Car('Roadster', 210.0, 'gasoline', 2),
)"""

    target_file = tmpdir.join("target.py")
    renew.serialize(str(target_file), blenda=driver_1, trytka=driver_2)

    assert target_file.read() == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from living.things import persons
from that.things import cars

blenda = persons.Driver('Blenda', 'Klapa', cars.Car('Truck', 120.0, 'diesel', 2))

trytka = persons.Driver(
    'Trytka',
    'Blotnick',
    cars.Car('Van', 145.0, 'diesel', 7, 'silver'),
    cars.Car('Roadster', 210.0, 'gasoline', 2),
)
"""


def test_reproducible_base():
    class A(object):
        pass

    class B(A, renew.Mold):
        def __init__(self, kak):
            self.kak = kak

    class C(B):
        def __init__(self, *tak):
            self.tak = tak
            super(C, self).__init__("kak_value")

    assert repr(B(3)) == "B(3)"
    assert repr(C(4, 5, 6)) == "C(4, 5, 6)"


def test_other_stuff_from_readme():
    class ThatNiceClass(renew.Mold):
        # manual implementation of __init__ is needed. Constructor_arguments
        # have to be actual names of this class attributes
        def __init__(self, f_a, f_b, *f_c, **f_d):
            self.f_a, self.f_b, self.f_c, self.f_d = f_a, f_b, f_c, f_d

    c = ThatNiceClass(1, 2, 3, 4, five=5, six=6)

    assert repr(c) == "ThatNiceClass(1, 2, 3, 4, five=5, six=6)"
    assert repr(c) == repr(eval(repr(c)))  # pure reproduction

    class SecondClass(renew.Mold):
        _cls_namespace = "foo"

        def __init__(self, one, two="number two", three=None):
            self.one, self.two, self.three = one, two, three

    s1 = SecondClass(1)
    s2 = SecondClass(3.14159, "non default")
    s3 = SecondClass("Lorem ipsum dolor sit amet, consectetur adipiscing elit")
    s4 = SecondClass(4, three=ThatNiceClass(1, 2, 3, 4, five=5, six=6))

    d = ThatNiceClass(s1, s2, lorem=s3, im_nesting=s4)

    assert repr(d) == """\
ThatNiceClass(
    foo.SecondClass(1),
    foo.SecondClass(3.14159, 'non default'),
    im_nesting=foo.SecondClass(4, three=ThatNiceClass(1, 2, 3, 4, five=5, six=6)),
    lorem=foo.SecondClass('Lorem ipsum dolor sit amet, consectetur adipiscing elit'),
)"""


class OD(renew.Mold):
    def __init__(self, a, *items):
        self.a = a
        self.items = OrderedDict(items)


class OD2(OD):
    pass


class OD3(OD):
    def __init__(self, a, *items, **kwargs):
        self.kwargs = kwargs
        super(OD3, self).__init__(a, *items)


def test_ordered_dict():
    od = OD(3.14159)

    assert repr(od) == "OD(3.14159)"
    od.items['bb'] = 1
    od.items['aa'] = 2
    assert repr(od) == "OD(3.14159, ('bb', 1), ('aa', 2))"


@pytest.mark.parametrize("cls", [OD, OD2, OD3])
def test_eq(cls):
    this = cls(3.14159, ("a", "1"), ("b", "2"))
    same = cls(3.14159, ('a', '1'), ('b', '2'))
    different = cls(3.14159, ('a', '1'))

    assert this == same
    assert this != different
    assert this != 3.14159
    assert not this == different
    assert not this != same
    assert not this == 3.14159


def test_repr_tuples():
    a = OD(("letter a",), ((1, 2), 12), ((4,), 4))
    assert a == OD(("letter a",), ((1, 2), 12), ((4,), 4))

    assert repr(a) == "OD(('letter a',), ((1, 2), 12), ((4,), 4))"
