import pytest

from renew._inspection import ArgsInspect


def test_forgot_to_write_init():
    class A(object):
        pass

    msg = "The class A does not implement constructor."
    with pytest.raises(TypeError, match=msg):
        ArgsInspect.from_type(A)


def spec(args, defaults):
    return ArgsInspect(args, None, None, defaults)


@pytest.mark.parametrize('spec, expected_result', [
    [spec(("a", "b", "c"), ()), (('a', False, None), ('b', False, None), ('c', False, None))],
    [spec(("a", "b", "c"), (3,)), (('a', False, None), ('b', False, None), ('c', True, 3))],
    [spec(("a", "b", "c"), (2, 3)), (('a', False, None), ('b', True, 2), ('c', True, 3))],
    [spec(("a", "b", "c"), (1, None, 3)), (('a', True, 1), ('b', True, None), ('c', True, 3))],
])
def test_bind_defaults(spec, expected_result):
    assert spec._bind_defaults() == expected_result


class NoArgs(object):
    def __init__(self):
        pass


def test_collect_no_attributes():
    weird_object = NoArgs()
    spec = ArgsInspect.from_type(NoArgs)
    assert len(spec) == 0
    assert list(spec) == []
    assert list(spec.cls_arguments(weird_object)) == []


class Defaults(object):
    def __init__(self, first, second=22, third=33, fourth=4.4, **others):
        self.first, self.second, self.third, self.fourth, self.others = first, second, third, fourth, others


def test_collect_defaults():
    spec = ArgsInspect.from_type(Defaults)
    assert len(spec) == 5
    assert list(spec) == ['first', 'second', 'third', 'fourth', 'others']

    def check(obj, *ref):
        assert list(spec.cls_arguments(obj)) == list(ref)

    check(Defaults(1), (None, 1))
    check(Defaults(1, 22), (None, 1))
    check(Defaults(1, 22, 33), (None, 1))
    check(Defaults(1, 22, 33, None), (None, 1), ("fourth", None))
    check(Defaults(1, 10, 20, 30), (None, 1), (None, 10), (None, 20), (None, 30))
    check(Defaults(None, None, None, None), (None, None), (None, None), (None, None), (None, None))
    check(Defaults(1, fourth='d', second='b', third=None), (None, 1), (None, 'b'), (None, None), (None, 'd'))
    check(Defaults(1, fourth='droppin'), (None, 1), ('fourth', 'droppin'))


class PKw(object):
    def __init__(self, a, *b, **c):
        self.a = a
        self.b = b
        self.c = c


def test_collect_attributes_1():
    weird_object = PKw("aaa", "b1", "b2", c1="cc1", c2="cc2", c=None)
    spec = ArgsInspect.from_type(PKw)
    assert len(spec) == 3
    assert list(spec) == ['a', 'b', 'c']
    result = list(spec.cls_arguments(weird_object))

    assert result == [(None, 'aaa'), (None, 'b1'), (None, 'b2'), ('c', None), ('c1', 'cc1'), ('c2', 'cc2')]
