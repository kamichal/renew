import collections
import os

import pytest

import renew


@pytest.fixture
def render(tmpdir):
    out_file = tmpdir.join("illi_ant.py")

    def _render(*args, **kwargs):
        renew.serialize(str(out_file), *args, **kwargs)
        return out_file.read()

    return _render


class Thing(renew.Mold):
    _cls_namespace = "foo"
    _cls_dependency = "package"

    def __init__(self, one, *two):
        self.one = one
        self.two = two


class Thong(renew.Mold):
    _cls_namespace = "thongowo"

    def __init__(self, value):
        self.value = value


def test_serialize_kw(tmpdir):
    target = tmpdir.join("that.py")
    obj = Thing(1, 2)
    odict_ = collections.OrderedDict([("one", 1), ("two", 2)])
    renew.serialize(str(target), this_instance=obj, ordered=odict_)

    assert target.read() == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from collections import OrderedDict
from package import foo

ordered = OrderedDict([('one', 1), ('two', 2)])

this_instance = foo.Thing(1, 2)
"""


def test_serialize_in_order(tmpdir):
    this = Thing(1, 2)
    second = Thing("This is a python-serialized instance", 34, 35, 36)
    target = tmpdir.join("that.py")
    renew.serialize_in_order(
        str(target),
        ('this_instance', this),
        ('second_instance', second),
        ('thong_instance', Thong('value for this instance')),
    )

    assert target.read() == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from package import foo
import thongowo

this_instance = foo.Thing(1, 2)

second_instance = foo.Thing('This is a python-serialized instance', 34, 35, 36)

thong_instance = thongowo.Thong('value for this instance')
"""


class Base(object):
    def __init__(self, a):
        self.a = a


def test_deps_n_namespaces(tmpdir):
    class A(renew.Mold):
        _cls_namespace = "namespace_a"

        def __init__(self, a):
            self.a = a

    class B(A):
        _cls_namespace = "namespace_b"
        _cls_dependency = "dependency_b"

    class C(renew.Mold):
        _cls_dependency = "dependency_c"

        def __init__(self, a):
            self.a = a

    class D(renew.Mold):
        def __init__(self, a):
            self.a = a

    target = tmpdir.join("that.py")
    renew.serialize(str(target), a=A(1), b=B(3), c=C(5), d=D(7))
    assert target.read() == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from dependency_b import namespace_b
from dependency_c import C
import namespace_a
from renew.tests.test_serializer import D

a = namespace_a.A(1)

b = namespace_b.B(3)

c = C(5)

d = D(7)
"""


def test_import_from_same_dependency(render):
    class ZZZ(renew.Mold):
        _cls_namespace = "namespace_for_z"

    class A8A(renew.Mold):
        _cls_dependency = "dependency_for_a"

        def __init__(self, a):
            self.a = a

    class B8B(A8A):
        pass

    class C8C(B8B):
        pass

    class D8D(renew.Mold):
        _cls_namespace = "namespace_d"
        _cls_dependency = "dep_dor_d8d"

    assert render(("zed", ZZZ()), _aa=A8A(23), _bb=B8B("da value"), _cc=C8C(None), _dd=D8D()) == """\
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been created with renew.
# A py-pickling tool: https://pypi.org/project/renew/

from dep_dor_d8d import namespace_d
from dependency_for_a import A8A
from dependency_for_a import B8B
from dependency_for_a import C8C
import namespace_for_z

zed = namespace_for_z.ZZZ()

_aa = A8A(23)

_bb = B8B('da value')

_cc = C8C(None)

_dd = namespace_d.D8D()
"""


def test_serializer_assertion(mocker):
    mocker.patch.object(renew._serializer.codecs, "open")

    fake_file = os.path.join("not_existing_path_test", "file.py")
    with pytest.raises(ValueError, match="Target dir does not exist: 'not_existing_path_test'."):
        renew.serialize(fake_file, empty_list=[])
