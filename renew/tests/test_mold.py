from collections import OrderedDict

import pytest

from renew import Mold


class MoldA(Mold):
    def __init__(self, one, *two, **three):
        self.one = one
        self.two = set(two)
        self.three = OrderedDict(three)


def test_meta_a():
    a = MoldA(None)
    assert repr(a) == "MoldA(None)"

    assert a._cls_dependency is None
    assert a._cls_namespace is None
    assert a.__slots__ == ("one", "two", "three")
    assert a._cls_fields == ("one", "two", "three")

    assert [getattr(a, f) for f in a._cls_fields] == [None, set(), OrderedDict()]
    assert list(a) == [None, set(), OrderedDict()]

    msg = "'MoldA' object has no attribute 'that_is_not_in_slots'"
    with pytest.raises(AttributeError, match=msg):
        a.that_is_not_in_slots = 23


def test_fields_a():
    a = MoldA(1, 2, (3, 4), val1=3.14159, val2=False)

    assert a.one == 1
    assert a.two == {2, (3, 4)}
    assert dict(a.three) == dict(val1=3.14159, val2=False)
    assert isinstance(a.three, OrderedDict)

    a1, a2, a3 = a
    assert list(a) == [a1, a2, a3]


class MoldBNoCtor(MoldA):
    _cls_namespace = "bang"


def test_meta_b():
    b = MoldBNoCtor(True, forward=True, opposite=False)
    assert b._cls_dependency is None
    assert b._cls_namespace == "bang"
    assert b.__slots__ == ()
    assert b._cls_fields == ("one", "two", "three")

    msg = "'MoldBNoCtor' object has no attribute 'that_is_not_in_slots'"
    with pytest.raises(AttributeError, match=msg):
        b.that_is_not_in_slots = 23


def test_fields_b():
    b = MoldBNoCtor(True, forward=True, opposite=False)
    b1, b2, b3 = b

    assert b.one is True is b1
    assert b.two == set() == b2
    assert b.three == dict(forward=True, opposite=False) == b3
    assert isinstance(b.three, OrderedDict)
    assert list(b) == [b1, b2, b3]


class MoldC(MoldBNoCtor):
    _cls_namespace = "ccc"
    _cls_dependency = "origin_of_c"

    def __init__(self, zero, one):
        self.zero = zero
        two = 2, 22
        three = {"fixed": "value"}
        super(MoldC, self).__init__(one, *two, **three)


def test_meta_c():
    c = MoldC(0, 1)
    assert c._cls_dependency == "origin_of_c"
    assert c._cls_namespace == "ccc"
    assert c.__slots__ == ("zero",)
    assert c._cls_fields == ("zero", "one", "two", "three")
    assert repr(c) == "ccc.MoldC(0, 1)"

    msg = "'MoldC' object has no attribute 'that_is_not_in_slots'"
    with pytest.raises(AttributeError, match=msg):
        c.that_is_not_in_slots = 23


def test_fields_c():
    c = MoldC("value for arg zero", ["list", "of", "values", "for", "argument", "one"])
    c0, c1, c2, c3 = c
    assert c.zero == "value for arg zero" == c0
    assert c.one == ["list", "of", "values", "for", "argument", "one"] == c1
    assert c.two == {2, 22} == c2
    assert c.three == {"fixed": "value"} == c3
    assert isinstance(c.three, OrderedDict)
    assert repr(c) == "ccc.MoldC('value for arg zero', ['list', 'of', 'values', 'for', 'argument', 'one'])"


class MoldD(MoldBNoCtor):
    _cls_namespace = "knights.ni"

    def __init__(self, zero, one, two, three):
        self.zero = zero
        super(MoldD, self).__init__(one, *two, **three)


def test_meta_d():
    d = MoldD("value typed: zero", 1, (2, 3), three={"kw_arg": False})
    assert repr(d) == "knights.ni.MoldD('value typed: zero', 1, {2, 3}, OrderedDict([('kw_arg', False)]))"

    assert d._cls_dependency is None
    assert d._cls_namespace == "knights.ni"
    assert d.__slots__ == ("zero",)
    assert d._cls_fields == ("zero", "one", "two", "three")

    msg = "'MoldD' object has no attribute 'that_is_not_in_slots'"
    with pytest.raises(AttributeError, match=msg):
        d.that_is_not_in_slots = 23


def test_repr_and_eq_a():
    this = MoldA(12, 23, 34, 45, other="stuff")
    same = MoldA(12, 23, 34, 45, other="stuff")

    assert this is this
    assert this is not same
    assert not (this is same)

    assert hash(this) == hash(same)

    assert repr(this) == "MoldA(12, 23, 34, 45, other='stuff')"
    assert repr(this) == repr(eval(repr(this)))

    assert this == eval(repr(this))
    assert not this != eval(repr(this))

    assert this != 3.14159
    assert not this == 3.14159

    # check compare same type, different values
    different_1 = MoldA(12, 23, 34, 0, other="stuff")
    assert this != different_1
    assert not this == different_1
    assert hash(this) != hash(different_1)

    different_2 = MoldA(12, 23, 34, 45, other="different")
    assert this != different_2
    assert not this == different_2
    assert hash(this) != hash(different_2)

    # check same values when other is similar type
    same_value_lower_type = MoldBNoCtor(12, 23, 34, 45, other="stuff")
    assert hash(this) != hash(same_value_lower_type)

    assert this != same_value_lower_type
    assert not this == same_value_lower_type
    assert same_value_lower_type != this
    assert not same_value_lower_type == this


class ChangedInterface(MoldC):
    def __init__(self, foo, bar, **one):
        self.foo = foo
        self.bar = bar
        super(ChangedInterface, self).__init__("zero is required", one)


def test_changed_interface():
    e = ChangedInterface(1.0, 2.0, bike=False, car=True)

    assert e._cls_dependency == "origin_of_c"
    assert e._cls_namespace == "ccc"

    assert repr(e) == "ccc.ChangedInterface(1.0, 2.0, bike=False, car=True)"


def test_diamond_mro():
    class ABaseType(Mold):

        def __init__(self, *a):
            self.a = a

    class B(ABaseType):
        _cls_namespace = "BB"

        def __init__(self, *b):
            self.b = b
            super(B, self).__init__()

    class C(ABaseType):
        _cls_namespace = "CC"

    class D(B):
        def __init__(self, *d):
            self.d = d
            super(D, self).__init__()

    class Tip1(C, D):
        pass

    class Tip2(D, C):
        def __init__(self, e, *d):
            self.e = e
            super(Tip2, self).__init__(*d)

    ABaseType._cls_namespace = "different"
    B._cls_namespace = "different"
    objects = [ABaseType(1), B(2), C(3), D(4), Tip1(5), Tip2(6, 1)]

    assert dict(get_attrs(objects, "__slots__")) == {
        'ABaseType': ('a',),
        'B': ('b',),
        'C': (),
        'D': ('d',),
        'Tip1': (),
        'Tip2': ('e',),
    }
    assert dict(get_attrs(objects, "_cls_fields")) == {
        'ABaseType': ('a',),
        'B': ('b', 'a'),
        'C': ('a',),
        'D': ('d', 'b', 'a'),
        'Tip1': ('a', 'd', 'b'),
        'Tip2': ('e', 'd', 'b', 'a'),
    }

    assert dict(get_attrs(objects, "_cls_namespace")) == {
        'ABaseType': 'different',
        'B': 'different',
        'C': 'CC',
        'D': 'different',
        'Tip1': 'CC',
        'Tip2': 'different',
    }
    assert dict(get_attrs(objects, "make_py_reference")) == {
        'Tip2': 'different.Tip2',
        'D': 'different.D',
        'Tip1': 'CC.Tip1',
        'ABaseType': 'different.ABaseType',
        'B': 'different.B', 'C': 'CC.C',
    }
    assert repr(objects) == "[different.ABaseType(1), different.B(2), CC.C(3), different.D(4), CC.Tip1(5), " \
                            "different.Tip2(6, 1)]"


def get_attrs(objects, name):
    for c in objects:
        yield c.__class__.__name__, getattr(c, name, None)


def test_using_properties():
    class Base(Mold):
        def __init__(self, one):
            self.one = one

    class ReadOnly(Base):
        def __init__(self, one, *two, **three):
            super(ReadOnly, self).__init__(one)
            self._two = list(two)
            self.three = three

        @property
        def two(self):
            return self._two

    class Derived(ReadOnly):
        def __init__(self, extra_param, one, *two, **three):
            super(Derived, self).__init__(one, *two, **three)
            self._extra_param = extra_param

        @property
        def extra_param(self):
            return self._extra_param

        @extra_param.setter
        def extra_param(self, new_value):
            self._extra_param = new_value

    base = Base("ONE_1")
    read_only = ReadOnly("ONE_R", "TWO_R", three="something")
    derived = Derived("EXTRA", "ONE_3", "TWO_3", t="T")

    objects = [base, read_only, derived]
    assert dict(get_attrs(objects, "__slots__")) == {
        'Base': ('one',),
        'ReadOnly': ('_two', 'three'),
        'Derived': ('_extra_param',),
    }
    assert dict(get_attrs(objects, "_cls_fields")) == {
        'Base': ('one',),
        'ReadOnly': ('two', 'three', 'one'),
        'Derived': ('extra_param', 'two', 'three', 'one'),
    }

    assert read_only.two == ['TWO_R']
    assert derived.two == ['TWO_3']
    assert repr(base) == "Base('ONE_1')"
    assert repr(read_only) == "ReadOnly('ONE_R', 'TWO_R', three='something')"
    assert repr(derived) == "Derived('EXTRA', 'ONE_3', 'TWO_3', t='T')"


def test_no_slots():
    class A1(Mold):

        def __init__(self, a):
            self.a = a

    class A2(A1):
        _cls_make_slots = False

    class B1(Mold):
        _cls_make_slots = False

        def __init__(self, b):
            self.b = b

    class B2(B1):
        _cls_make_slots = True

    a1, a2, b1, b2 = A1(1), A2(2), B1(30), B2(40)
    assert a1.a == 1
    assert a2.a == 2
    assert b1.b == 30
    assert b2.b == 40

    msg = "'A1' object has no attribute 'non_slot'"
    with pytest.raises(AttributeError, match=msg):
        a1.non_slot = "anything, is bad"

    a2.non_slot = "anything, but no raise, pass test"
    b1.non_slot = "anything, but no raise, pass test"
    b2.non_slot = "__slots__ already broken, so this is also allowed"

    assert a2 == A2(2)
    assert b1 == B1(30)


def test_extra_slots():
    class A1(Mold):
        _extra_slots = ("bb",)

        def __init__(self, aa):
            self.aa = aa

    class A2(A1):
        pass

    class A3(A2):
        _extra_slots = "thing"

    class A4(A3):
        def __init__(self, cc, aa):
            self.cc = cc
            super(A4, self).__init__(aa)

    assert A1._cls_fields == ('aa',)
    assert A2._cls_fields == ('aa',)
    assert A3._cls_fields == ('aa',)
    assert A4._cls_fields == ('cc', 'aa',)

    a3 = A3(123)
    equal_to_a3 = A3(123)

    # No raise, no errors, slots are accessible
    assert a3.aa == 123
    a3.bb = "bb value"
    a3.thing = 321

    assert a3 == equal_to_a3

    with pytest.raises(AttributeError, match="'A3' object has no attribute 'other'"):
        a3.other = "Not exist."


def test_quite_long_strings(is_py2):
    long_text = (
        u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
        u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. '
        u'Libero nunc consequat interdum varius sit'
    )

    assert repr(MoldA(long_text)) == """\
MoldA(
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    u'nunc consequat interdum varius sit'
)""" if is_py2 else """\
MoldA(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    'nunc consequat interdum varius sit'
)"""

    assert repr(MoldA(long_text, long_text)) == """\
MoldA(
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    u'nunc consequat interdum varius sit',
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    u'nunc consequat interdum varius sit'
)""" if is_py2 else """\
MoldA(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    'nunc consequat interdum varius sit',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    'nunc consequat interdum varius sit'
)"""

    assert repr(MoldA(long_text, "short one")) == """\
MoldA(
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    u'nunc consequat interdum varius sit',
    'short one',
)""" if is_py2 else """\
MoldA(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    'nunc consequat interdum varius sit',
    'short one',
)"""

    assert repr(MoldA(1, key_a="1 " + long_text, key_two="2 " + long_text)) == """\
MoldA(
    1,
    key_a=u'1 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor '
    u'incididunt ut labore et dolore magna aliqua. Libero nunc consequat interdum varius '
    u'sit. Libero nunc consequat interdum varius sit',
    key_two=u'2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor '
    u'incididunt ut labore et dolore magna aliqua. Libero nunc consequat interdum varius '
    u'sit. Libero nunc consequat interdum varius sit'
)""" if is_py2 else """\
MoldA(
    1,
    key_a='1 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor '
    'incididunt ut labore et dolore magna aliqua. Libero nunc consequat interdum varius '
    'sit. Libero nunc consequat interdum varius sit',
    key_two='2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor '
    'incididunt ut labore et dolore magna aliqua. Libero nunc consequat interdum varius '
    'sit. Libero nunc consequat interdum varius sit'
)"""
