import collections
from collections import OrderedDict

import pytest

import renew


@pytest.mark.parametrize("obj", [
    (), [], {}, set(), 'str', (1,), [1], {1}, {1: 2},
])
def test_repr_of_simple_types(obj):
    assert eval(renew.reproduction(obj)) == obj


def test_ordered_dict():
    obj = OrderedDict()
    assert eval(renew.reproduction(obj)) == obj
    assert renew.reproduction(obj) == "OrderedDict([])"

    obj = OrderedDict([(3.14159, 2), (1, 0)])
    assert eval(renew.reproduction(obj)) == obj
    assert renew.reproduction(obj) == "OrderedDict([(3.14159, 2), (1, 0)])"


def test_long_list():
    object_ = [[100 ** i + x ** 4 for x in range(10)] for i in range(3)]
    assert renew.reproduction(object_) == """\
[
    [1, 2, 17, 82, 257, 626, 1297, 2402, 4097, 6562],
    [100, 101, 116, 181, 356, 725, 1396, 2501, 4196, 6661],
    [10000, 10001, 10016, 10081, 10256, 10625, 11296, 12401, 14096, 16561],
]"""


def test_object_with_long_strings(is_py2):
    long_text = (
        u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
        u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. '
        u'Libero nunc consequat interdum varius sit'
    )
    plain_tuple = (long_text, long_text)

    assert renew.reproduction(plain_tuple) == """\
(
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    u'nunc consequat interdum varius sit',
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    u'nunc consequat interdum varius sit'
)""" if is_py2 else """\
(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    'nunc consequat interdum varius sit',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    'nunc consequat interdum varius sit'
)"""

    plain_list = [long_text, long_text]

    assert renew.reproduction(plain_list) == """\
[
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    u'nunc consequat interdum varius sit',
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    u'nunc consequat interdum varius sit'
]""" if is_py2 else """\
[
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    'nunc consequat interdum varius sit',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Libero '
    'nunc consequat interdum varius sit'
]"""


def test_repr_namedtuple():
    A = collections.namedtuple('A', 'one, two, three')
    a = A("Lorem ipsum dolor sit amet, ", "consectetur adipiscing elit, ", "sed do eiusmod tempor incididunt")

    assert renew.reproduction(a) == """\
(
    'Lorem ipsum dolor sit amet, ',
    'consectetur adipiscing elit, ',
    'sed do eiusmod tempor incididunt',
)"""
    assert eval(renew.reproduction(a)) == a
    b = A(None, -3, a)

    assert renew.reproduction(b) == """\
(
    None,
    -3,
    (
        'Lorem ipsum dolor sit amet, ',
        'consectetur adipiscing elit, ',
        'sed do eiusmod tempor incididunt',
    ),
)"""


@pytest.fixture
def lorem_with_breaks():
    return "\n".join([
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt "
        "ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Maecenas accumsan "
        "lacus vel facilisis:",
        "  - Dui ut ornare,",
        "  - Lectus,",
        "  - Malesuada pellentesque,",
        "",
        "",
        "Elit eget gravida cum sociis natoque penatibus et. Netus et malesuada fames ac turpis egestas sed.",
        "Egestas integer eget aliquet.",
    ])


def test_repr_long_string(lorem_with_breaks, is_py2):
    assert renew.reproduction(lorem_with_breaks) == r"""(
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Maecenas '
    u'accumsan lacus vel facilisis:\n  - Dui ut ornare,\n  - Lectus,\n  - Malesuada pellentesque,\n\n\nElit '
    u'eget gravida cum sociis natoque penatibus et. Netus et malesuada fames ac turpis '
    u'egestas sed.\nEgestas integer eget aliquet.'
)""" if is_py2 else r"""(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt '
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit. Maecenas '
    'accumsan lacus vel facilisis:\n  - Dui ut ornare,\n  - Lectus,\n  - Malesuada pellentesque,\n\n\nElit '
    'eget gravida cum sociis natoque penatibus et. Netus et malesuada fames ac turpis '
    'egestas sed.\nEgestas integer eget aliquet.'
)"""


def test_repr_text_short_lines(is_py2):
    that_text = """\
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit.
Maecenas accumsan lacus vel facilisis:
 - Dui ut ornare,
 - Lectus,
 - Malesuada pellentesque
"""
    assert renew.reproduction(that_text) == r"""(
    u'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do\n'
    u'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit.\n'
    u'Maecenas accumsan lacus vel facilisis:\n'
    u' - Dui ut ornare,\n'
    u' - Lectus,\n'
    u' - Malesuada pellentesque\n'
    u''
)""" if is_py2 else r"""(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do\n'
    'ut labore et dolore magna aliqua. Libero nunc consequat interdum varius sit.\n'
    'Maecenas accumsan lacus vel facilisis:\n'
    ' - Dui ut ornare,\n'
    ' - Lectus,\n'
    ' - Malesuada pellentesque\n'
    ''
)"""
