# -*- encoding: utf-8 -*-
import os

import pytest

import renew
from renew.tests import common_molds


class NastyStore(renew.PyStorage):
    one = renew.Label(common_molds.One)
    other = renew.Reference(common_molds.Three)


NASTY_CHARACTERS = [u"pąk", u"kór", u"⛽", u" … and ☃ and some others: ⚽ ⛅."]
PANGRAMS = dict(
    ku=u"Jalapeño", en="The quick brown fox jumps over the lazy dog.",
    pl=u"Pchnąć w tę łódź jeża lub ośm skrzyń fig.",
    cz=u"Nechť již hříšné saxofony ďáblů rozezvučí síň úděsnými tóny waltzu, tanga a quickstepu.",
    hu=u"Jámbor célú, öv ügyű ex-qwan ki dó-s főz, puhít.",
    gr=u"ζίες καὶ μυρτιὲς δὲν θὰ βρῶ πιὰ στὸ χρυσαφὶ ξέφωτο.",
    ru=u"Любя, съешь щипцы, — вздохнёт мэр, — кайф жгуч.",
)


@pytest.fixture
def ref_store():
    this_dir = os.path.dirname(os.path.abspath(__file__))
    store_path = os.path.join(this_dir, "_ref_store")
    if not os.path.isdir(store_path):
        pytest.fail("Missing reference files.")

    required = ["__init__.py", "_sub_other.py"]
    for n in required:
        if not os.path.isfile(os.path.join(store_path, n)):
            pytest.fail("missing %s reference file" % n)
    return store_path


def test_unicode_serialization(tmpdir):
    store_path = str(tmpdir.join("kkk"))

    db = NastyStore(store_path, create=True)

    with db.updating_db():
        db.one = common_molds.One(*NASTY_CHARACTERS)
        db.other = common_molds.Three(**PANGRAMS)

    recreated = NastyStore(store_path)

    assert recreated is not db
    assert recreated.one is not db.one
    assert recreated.one == db.one
    assert recreated.other is not db.other
    assert recreated.other == db.other


def test_reading_a_store(ref_store):
    st = NastyStore(ref_store)
    assert st.one == common_molds.One(*NASTY_CHARACTERS)
    assert st.other == common_molds.Three(**PANGRAMS)
