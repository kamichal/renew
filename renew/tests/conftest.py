import pprint
import sys

import pytest
import six

DIFF_TEMPLATE = u"""
--- (left side of an assertion {ltype}):
{left}---
--- does not match currently defined reference:
--- (right {rtype}):
{right}---

"""


@pytest.fixture
def is_py2():
    return sys.version[0] == '2'


def pytest_assertrepr_compare(op, left, right):
    if op == "==":
        report_string_eq(left, right)


def report_string_eq(left, right):
    repr_left = pprint.pformat(left) if not isinstance(left, six.string_types) else left
    repr_right = pprint.pformat(right) if not isinstance(right, six.string_types) else right

    if left != right:
        if len(repr_left) > 10 or len(repr_right) > 10:
            print(DIFF_TEMPLATE.format(left=repr_left, right=repr_right, ltype=type(left), rtype=type(right)))
